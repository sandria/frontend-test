//slide when mouse drag
$(".carousel").swipe({
    swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
        if (direction == 'left') jQuery(this).carousel('next');
        if (direction == 'right') jQuery(this).carousel('prev');
    },
});

//adding + and - to navbar dropwodn at mobile version
function ready() {
    if ($(window).width() < 992) {
        // pages dropdown

        $(".plus-icon-pages").show();
        $(".dropdown-pages.dropdown").on("show.bs.dropdown", function (event) {
            $(".minus-icon-pages").show();
            $(".plus-icon-pages").hide();
        });
        $(".dropdown-pages.dropdown").on("hide.bs.dropdown", function (event) {
            $(".plus-icon-pages").show();
            $(".minus-icon-pages").hide();
        });
        // blog dropdown
        $(".plus-icon-blog").show();
        $(".dropdown-blog.dropdown").on("show.bs.dropdown", function (event) {
            $(".minus-icon-blog").show();
            $(".plus-icon-blog").hide();
        });
        $(".dropdown-blog.dropdown").on("hide.bs.dropdown", function (event) {
            $(".plus-icon-blog").show();
            $(".minus-icon-blog").hide();
        });
    }
}

$(window).resize(function () {
    if ($(window).width() < 992) {
        // pages dropdown

        $(".plus-icon-pages").show();
        $(".dropdown-pages.dropdown").on("show.bs.dropdown", function (event) {
            $(".minus-icon-pages").show();
            $(".plus-icon-pages").hide();
        });
        $(".dropdown-pages.dropdown").on("hide.bs.dropdown", function (event) {
            $(".plus-icon-pages").show();
            $(".minus-icon-pages").hide();
        });
        // blog dropdown
        $(".plus-icon-blog").show();
        $(".dropdown-blog.dropdown").on("show.bs.dropdown", function (event) {
            $(".minus-icon-blog").show();
            $(".plus-icon-blog").hide();
        });
        $(".dropdown-blog.dropdown").on("hide.bs.dropdown", function (event) {
            $(".plus-icon-blog").show();
            $(".minus-icon-blog").hide();
        });
    }
})




