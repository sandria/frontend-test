<?php
include "connection/connection.php";
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Teko:700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Paytone+One&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/effect.css">

    <title>Nibble</title>
</head>

<body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="#"><img src="img/nibble_logo.png" alt="brand" class="img-responsive"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" onclick="ready()">
            <span class="navbar-toggler-icon">
                <img src="img/menu.png" alt="">
            </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
        </div>
    </nav>
    <!-- END navbar -->

    <!-- Carousel -->
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="img/Background.jpg" class="d-block w-100" alt="first">
                <div class="carousel-caption">
                    <h1 class="display-3">BUILD UP YOUR</h1>
                    <h1 class="display-1">STRENGTH</h1>
                    <h1 class="display-5">Build Your Body and Fitness with Professional Touch</h1>
                    <button type="button" class="btn btn-danger btn-caption">JOIN US</button>
                </div>
            </div>
            <div class="carousel-item">
                <img src="img/Background2.jpg" class="d-block w-100" alt="second">
                <div class="carousel-caption">
                    <h1 class="display-3">BUILD UP YOUR</h1>
                    <h1 class="display-1">BODY SHAPE</h1>
                    <h1 class="display-5">Build Your Body and Fitness with Professional Touch</h1>
                    <button type="button" class="btn btn-danger btn-caption">JOIN US</button>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span><img src="img/previous.png" alt="arrow"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span><img src="img/next.png" alt=""></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- end Carousel -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src='//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script>
    <script src="js/effect.js"></script>
    <script src="js/ajax.js"></script>
</body>

</html>