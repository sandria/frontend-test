<?php
include "connection/connection.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php $result = mysqli_query($conn, "SELECT m.*, sm.* FROM menu m LEFT JOIN submenu sm ON m.id=sm.id GROUP BY m.id, sm.id"); ?>
    <?php
    $array = array();
    while ($row = mysqli_fetch_assoc($result)) :
        $array[] = $row;

    ?>
    <?php endwhile; ?>
    <ul class="navbar-nav mx-auto">
        <li class="nav-item active">
            <a class="nav-link" href="#"><?= $array[0]["menu_name"]; ?><span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><?= $array[1]["menu_name"]; ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><?= $array[2]["menu_name"]; ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><?= $array[3]["menu_name"]; ?></a>
        </li>
        <li class="nav-item dropdown dropdown-pages">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= $array[4]["menu_name"]; ?>
                <img src="img/down.png" alt="" class="down-arrow">
                <span class="plus-icon-pages" id="plus-icon">+</span>
                <span class="minus-icon-pages">-</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">
                <a class="dropdown-item" href="#"><?= $array[0]["submenu_name"]; ?></a>
                <a class="dropdown-item" href="#"><?= $array[1]["submenu_name"]; ?></a>
            </div>
        </li>
        <li class="nav-item dropdown dropdown-blog">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= $array[5]["menu_name"]; ?>
                <img src="img/down.png" alt="" class="down-arrow">
                <span class="plus-icon-blog">+</span>
                <span class="minus-icon-blog">-</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#"><?= $array[2]["submenu_name"]; ?></a>
                <a class="dropdown-item" href="#"><?= $array[3]["submenu_name"]; ?></a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><?= $array[6]["menu_name"]; ?></a>
        </li>
        <li class="nav-item">
            <button type="button" class="btn btn-danger btn-navbar">JOIN US</button>
        </li>
    </ul>

</body>

</html>